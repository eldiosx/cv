<h1 align="center">Curriculum Vitae</h1>

<div align="center">

[![HTML](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)](https://www.w3.org/TR/2014/REC-html5-20141028/) 
[![CSS](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)](https://www.w3.org/TR/2001/WD-css3-roadmap-20010523/) 

</div>

## Live demo: [Preview](https://eldiosx.gitlab.io/cv/)
